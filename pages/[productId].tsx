import Ract   from 'react';
import Stripe from 'stripe';
import { GetStaticPaths } from 'next';
import { GetStaticProps } from 'next';
import { Router }         from 'next/router';
import stripeConfig from '../config/stripe'

interface Propos { product: Stripe.Product; };

const Product: React.FC = ({ product }) => {
  return (
    <div>
      <h1>{product.name}</h1>
      <h2>{product.description}</h2>
      { product.images[0] && <img src={product.images[0]} alt={product.name}/> }
      <h5>R$ 0,00</h5>
    </div>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const STRIPE_API_KEY    = stripeConfig.stripe_api_key;
  const STRIPE_SECRET_KEY = stripeConfig.stripe_scret_key;

  const stripe = new Stripe(STRIPE_SECRET_KEY, {
    apiVersion: '2020-03-02',
  });

  /// const skus = await stripe.skus.list();
  /// console.log('[SKUS]');
  /// console.log(skus.data);

  const products = await stripe.products.list();
  console.log('[PRODUCTS]');
  console.log(products.data);

  const paths = products.data.map((product) => ({
    params: {
      productId: product.id,
    },
  }));
  console.log('[PATHS]');
  console.log(paths);

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const STRIPE_API_KEY    = stripeConfig.stripe_api_key;
  const STRIPE_SECRET_KEY = stripeConfig.stripe_scret_key;

  const stripe = new Stripe(STRIPE_SECRET_KEY, {
    apiVersion: '2020-03-02',
  });

  console.log('[PARAMS]');
  console.log(params);
  const { productId } = params;
  const product = await stripe.products.retrieve(productId as string);

  return {
    props: { product: product },
  };
};

export default Product;
